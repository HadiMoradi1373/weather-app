/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        primary:'#484B5B',
        secondry:'#232329',
        'fff':"#FFF",
        '868794':'#868794',
        'FFBD00':'#FFBD00',
        'F00':'#F00'
      },
      fontFamily:{
        'vazir':['Vazir']
      }
    }
  },
  plugins: [],
}

